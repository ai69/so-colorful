package colorlogo

func renderByLine(logo, fromColor, toColor string) string {
	if colorized, err := GradientByLine(logo, fromColor, toColor); err != nil {
		return logo
	} else {
		return colorized
	}
}

func renderByColumn(logo, fromColor, toColor string) string {
	if colorized, err := GradientByColumn(logo, fromColor, toColor); err != nil {
		return logo
	} else {
		return colorized
	}
}

func AlmostByColumn(logo string) string         { return renderByColumn(logo, "#DDD6F3", "#FAACA8") }
func AlmostByLine(logo string) string           { return renderByLine(logo, "#DDD6F3", "#FAACA8") }
func AnimalCrossingByColumn(logo string) string { return renderByColumn(logo, "#9DFFB0", "#81F1F7") }
func AnimalCrossingByLine(logo string) string   { return renderByLine(logo, "#9DFFB0", "#81F1F7") }
func BrokenHeartsByColumn(logo string) string   { return renderByColumn(logo, "#D9A7C7", "#FFFCDC") }
func BrokenHeartsByLine(logo string) string     { return renderByLine(logo, "#D9A7C7", "#FFFCDC") }
func CherryBlossomsByColumn(logo string) string { return renderByColumn(logo, "#FBD3E9", "#BB377D") }
func CherryBlossomsByLine(logo string) string   { return renderByLine(logo, "#FBD3E9", "#BB377D") }
func EveningNightByColumn(logo string) string   { return renderByColumn(logo, "#005AA7", "#FFFDE4") }
func EveningNightByLine(logo string) string     { return renderByLine(logo, "#005AA7", "#FFFDE4") }
func MiWatchByColumn(logo string) string        { return renderByColumn(logo, "#00A3FB", "#FF53FE") }
func MiWatchByLine(logo string) string          { return renderByLine(logo, "#00A3FB", "#FF53FE") }
func NelsonByColumn(logo string) string         { return renderByColumn(logo, "#F2709C", "#FF9472") }
func NelsonByLine(logo string) string           { return renderByLine(logo, "#F2709C", "#FF9472") }
func OceanSandByColumn(logo string) string      { return renderByColumn(logo, "#62C8C8", "#1462BE") }
func OceanSandByLine(logo string) string        { return renderByLine(logo, "#62C8C8", "#1462BE") }
func RainbowBlueByColumn(logo string) string    { return renderByColumn(logo, "#00F260", "#0575E6") }
func RainbowBlueByLine(logo string) string      { return renderByLine(logo, "#00F260", "#0575E6") }
func RoseWaterByColumn(logo string) string      { return renderByColumn(logo, "#E55D87", "#5FC3E4") }
func RoseWaterByLine(logo string) string        { return renderByLine(logo, "#E55D87", "#5FC3E4") }
