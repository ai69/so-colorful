package colorlogo

import (
	"bufio"
	"strings"

	"github.com/1set/gut/ystring"
	"github.com/lucasb-eyer/go-colorful"
	"github.com/muesli/termenv"
)

func GradientByLine(content, colorTop, colorBottom string) (string, error) {
	a, err := colorful.Hex(colorTop)
	if err != nil {
		return emptyStr, err
	}

	b, err := colorful.Hex(colorBottom)
	if err != nil {
		return emptyStr, err
	}

	total, err := countLines(content)
	if err != nil {
		return emptyStr, err
	}

	var (
		idx   = 0
		sb    = strings.Builder{}
		color = termenv.ColorProfile().Color
	)
	err = readByLine(content, func(l string) {
		percent := float64(idx) / float64(total)
		c := a.BlendLuv(b, percent).Hex()
		idx++
		sb.WriteString(termenv.String(l).Foreground(color(c)).String())
		sb.WriteString(ystring.NewLine)
	})
	if err != nil {
		return emptyStr, err
	}
	return sb.String(), nil
}

func GradientByColumn(content, colorLeft, colorRight string) (string, error) {
	a, err := colorful.Hex(colorLeft)
	if err != nil {
		return emptyStr, err
	}

	b, err := colorful.Hex(colorRight)
	if err != nil {
		return emptyStr, err
	}

	var (
		sb    = strings.Builder{}
		color = termenv.ColorProfile().Color
	)
	err = readByLine(content, func(l string) {
		runes := []rune(l)
		total := len(runes)
		for idx, char := range runes {
			percent := float64(idx) / float64(total)
			c := a.BlendLuv(b, percent).Hex()
			sb.WriteString(termenv.String(string(char)).Foreground(color(c)).String())
		}
		sb.WriteString(ystring.NewLine)
	})
	if err != nil {
		return emptyStr, err
	}
	return sb.String(), nil
}

var (
	emptyStr string

	readByLine = func(content string, callback func(line string)) (err error) {
		sc := bufio.NewScanner(strings.NewReader(content))
		for sc.Scan() {
			callback(sc.Text())
		}
		err = sc.Err()
		return
	}

	countLines = func(content string) (count int, err error) {
		err = readByLine(content, func(l string) {
			count++
		})
		return
	}
)
