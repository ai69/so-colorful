package colorcode

// Bash - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/b/bash.go
func Bash(content string) string {
	return Colorize("Bash", content)
}

// Batch - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/b/batch.go
func Batch(content string) string {
	return Colorize("Batch", content)
}

// C - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/c/c.go
func C(content string) string {
	return Colorize("C", content)
}

// CPP - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/c/cpp.go
func CPP(content string) string {
	return Colorize("C++", content)
}

// CSharp - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/c/csharp.go
func CSharp(content string) string {
	return Colorize("C#", content)
}

// Diff - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/d/diff.go
func Diff(content string) string {
	return Colorize("Diff", content)
}

// Go - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/g/go.go
func Go(content string) string {
	return Colorize("Go", content)
}

// JSON - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/j/json.go
func JSON(content string) string {
	return Colorize("JSON", content)
}

// Java - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/j/java.go
func Java(content string) string {
	return Colorize("Java", content)
}

// Python2 - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/p/python2.go
func Python2(content string) string {
	return Colorize("Python 2", content)
}

// Python3 - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/p/python.go
func Python3(content string) string {
	return Colorize("Python", content)
}

// TOML - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/t/toml.go
func TOML(content string) string {
	return Colorize("TOML", content)
}

// YAML - See reference: https://github.com/alecthomas/chroma/blob/master/lexers/y/yaml.go
func YAML(content string) string {
	return Colorize("YAML", content)
}
