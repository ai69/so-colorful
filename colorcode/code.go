package colorcode

import (
	"bytes"
	"fmt"

	"github.com/alecthomas/chroma/formatters"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
)

var (
	styleTheme = "monokai"
)

// Reference: https://xyproto.github.io/splash/docs/
func SetTheme(theme string) error {
	if style := styles.Get(theme); style == nil {
		return fmt.Errorf("no theme named %q", theme)
	}
	styleTheme = theme
	return nil
}

func Colorize(lexerName, content string) string {
	lexer := lexers.Get(lexerName)
	if lexer == nil {
		lexer = lexers.Fallback
	}

	style := styles.Get(styleTheme)
	if style == nil {
		style = styles.Fallback
	}

	formatter := formatters.Get("terminal256")
	if formatter == nil {
		formatter = formatters.Fallback
	}

	iterator, err := lexer.Tokenise(nil, content)
	if err != nil {
		return content
	}

	result := new(bytes.Buffer)
	if err := formatter.Format(result, style, iterator); err != nil {
		return content
	}
	return result.String()
}
