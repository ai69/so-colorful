# so colorful! とてもカラフル

## for code

```go
import (
	cc "bitbucket.org/ai69/so-colorful/colorcode"
)
```

## for ascii art

```go
import (
	cl "bitbucket.org/ai69/so-colorful/colorlogo"
)
```
