module bitbucket.org/ai69/so-colorful

go 1.16

require (
	github.com/1set/gut v0.0.0-20201117175203-a82363231997
	github.com/alecthomas/chroma v0.9.2
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/muesli/termenv v0.9.0
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
)
